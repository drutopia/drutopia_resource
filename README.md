<!-- writeme -->
Drutopia Resource
=================

Provides Resource content type and related configuration. A resource can be a file, such as a PDF, a link, such as a website URL, or an embedded video.

 * https://gitlab.com/drutopia/drutopia_resource
 * Issues: https://gitlab.com/drutopia/drutopia_resource/issues
 * Source code: https://gitlab.com/drutopia/drutopia_resource/tree/8.x-1.x
 * Keywords: resources, files, facets, filtering, search, content, drutopia
 * Package name: drupal/drutopia_resource


### Requirements

 * drupal/block_visibility_groups ^1.3
 * drupal/config_actions ^1.1
 * drupal/ctools ^3.4
 * drupal/drutopia_seo ^1.0
 * drupal/ds ^3.7
 * drupal/facets ^2
 * drupal/field_group ^3.0
 * drupal/pathauto ^1.8
 * drupal/token ^1.7
 * drupal/video_embed_field ^2.4


### License

GPL-2.0+

<!-- endwriteme -->
